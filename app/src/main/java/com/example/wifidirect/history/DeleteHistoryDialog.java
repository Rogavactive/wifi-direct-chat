package com.example.wifidirect.history;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.wifidirect.DB.ChatDataBase;

public class DeleteHistoryDialog extends DialogFragment {

    public interface DialogListener{
        void onDialogSuccess();
    }

    private static ChatDataBase db;
    private static DialogListener listener;

    public DeleteHistoryDialog(){
        super();
        db = ChatDataBase.getInstance(getContext());
    }

    public static DeleteHistoryDialog newInstance(String deviceName, long SSID){
        DeleteHistoryDialog fragment = new DeleteHistoryDialog();
        Bundle args = new Bundle();
        args.putString("deviceName", deviceName);
        args.putLong("SSID", SSID);
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        String deviceName = "Not found";
        long ssid = -1;
        if(getArguments()!=null){
            deviceName = getArguments().getString("deviceName","Not found");
            ssid = getArguments().getLong("SSID",-1);
        }
        long finalSsid = ssid;
        builder.setMessage("გსურთ " + deviceName + "-თან მიმოწერის წაშლა? ")
                .setPositiveButton("კი", (dialog, which) -> {
                    new SessionRemover(finalSsid).execute();
                }).setNegativeButton("არა", (dialog, which) -> {
                    //Do nothing
                });
        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (DialogListener) context;
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
    }

    private static class SessionRemover extends AsyncTask<Void, Void, Void>{

        private long ssid;

        SessionRemover(long ssid ){
            this.ssid = ssid;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            db.chatDao().deleteSession(ssid);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            listener.onDialogSuccess();
        }
    }
}
