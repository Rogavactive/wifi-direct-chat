package com.example.wifidirect.history;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.wifidirect.DB.ChatDataBase;

public class DeleteAllHistoryDialog extends DialogFragment {

    private static ChatDataBase db;
    private static DeleteHistoryDialog.DialogListener listener;

    public DeleteAllHistoryDialog(){
        super();
        db = ChatDataBase.getInstance(getContext());
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("გსურთ ისტორიის გასუფთავება? ")
                .setPositiveButton("კი", (dialog, which) -> {
                    new DeleteAllHistoryDialog.SessionRemover().execute();
                }).setNegativeButton("არა", (dialog, which) -> {
            //Do nothing
        });
        return builder.create();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (DeleteHistoryDialog.DialogListener) context;
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
    }

    private static class SessionRemover extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            db.chatDao().deleteAllSessions();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            listener.onDialogSuccess();
        }
    }

}
