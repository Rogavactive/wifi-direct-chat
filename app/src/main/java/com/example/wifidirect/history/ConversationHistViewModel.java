package com.example.wifidirect.history;

import android.app.Application;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.wifidirect.DB.ChatDataBase;
import com.example.wifidirect.DB.ConversationModel;

import java.util.List;

public class ConversationHistViewModel extends AndroidViewModel {

    private String deviceName;
    private long currentSSID;
    private static MutableLiveData<List<ConversationModel>> messages = new MutableLiveData<>();
    private static ChatDataBase db;

    public ConversationHistViewModel(@NonNull Application application) {
        super(application);
        db = ChatDataBase.getInstance(application.getApplicationContext());
    }

    void loadMessages(long sessionId) {
        currentSSID = sessionId;
        new MessageRetriever(sessionId).execute();
    }

    LiveData<List<ConversationModel>> getMessages() {
        return messages;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public long getCurrentSSID() {
        return currentSSID;
    }

    private static class MessageRetriever extends AsyncTask<Void,Void,Void>{

        private long ssid;

        MessageRetriever(long ssid){
            this.ssid = ssid;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            messages.postValue(db.chatDao().getConversations(ssid));
            return null;
        }
    }

}
