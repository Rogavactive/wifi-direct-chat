package com.example.wifidirect.history;

import android.app.Application;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.wifidirect.DB.ChatDataBase;
import com.example.wifidirect.DB.SessionsModel;

import java.util.List;

public class HistoryViewModel extends AndroidViewModel {

    private static MutableLiveData<List<SessionsModel>> history = new MutableLiveData<>();
    private static ChatDataBase db;

    public HistoryViewModel(@NonNull Application application) {
        super(application);
        db = ChatDataBase.getInstance(application.getApplicationContext());
    }

    LiveData<List<SessionsModel>> getHistory() {
        return history;
    }

    void loadData() {
        new HistoryLoader().execute();
    }

    private static class HistoryLoader extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            List<SessionsModel> data = db.chatDao().getSessions();
            history.postValue(data);
            return null;
        }

    }
}
