package com.example.wifidirect.history;

import android.content.Intent;
import android.os.Bundle;

import com.example.wifidirect.DB.SessionsModel;
import com.example.wifidirect.R;
import com.example.wifidirect.chat.ChatActivity;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import com.google.android.material.navigation.NavigationView;

import androidx.annotation.NonNull;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.util.List;

public class HistoryActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, DeleteHistoryDialog.DialogListener {

    private RecyclerView historyRecyclerView;
    private HistoryAdapter historyAdapter;
    private HistoryViewModel historyViewModel;
    private TextView notFoundLabel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        notFoundLabel = findViewById(R.id.notFoundLabel);
        notFoundLabel.setVisibility(View.GONE);

        historyViewModel = ViewModelProviders.of(this).get(HistoryViewModel.class);

        historyRecyclerView = findViewById(R.id.historyRecyclerView);
        historyRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        historyAdapter = new HistoryAdapter(new HistoryAdapter.ItemOnClickListener() {
            @Override
            public void onClick(Long sessionId, String deviceName) {
                Intent intent = new Intent(HistoryActivity.this, ConversationHistoryActivity.class);
                intent.putExtra("SSID", sessionId);
                intent.putExtra("deviceName", deviceName);
                startActivity(intent);
            }

            @Override
            public void onLongClick(Long sessionId, String deviceName) {
                DialogFragment deleteDialog = DeleteHistoryDialog.newInstance(deviceName,sessionId);
                deleteDialog.show(getSupportFragmentManager(),"deleteSingleSession");
            }
        });
        historyRecyclerView.setAdapter(historyAdapter);
        historyViewModel.getHistory().observe(this,this::setHistory);
        if(getSupportActionBar()!=null)
            getSupportActionBar().setTitle("ისტორია(0)");

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> {
            DialogFragment deleteDialog = new DeleteAllHistoryDialog();
            deleteDialog.show(getSupportFragmentManager(),"deleteSingleSession");
        });

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        historyViewModel.loadData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        historyViewModel.loadData();
    }

    private void setHistory(List<SessionsModel> sessionsModels) {
        historyAdapter.setHistory(sessionsModels);
        if(sessionsModels.size()==0){
            notFoundLabel.setVisibility(View.VISIBLE);
        }else{
            notFoundLabel.setVisibility(View.GONE);
        }
        if(getSupportActionBar()!=null)
            getSupportActionBar().setTitle("ისტორია(" + sessionsModels.size() + ")");
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.nav_chat) {
            Intent intent = new Intent(HistoryActivity.this, ChatActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_history) {
            historyViewModel.loadData();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onDialogSuccess() {
        historyViewModel.loadData();
    }
}
