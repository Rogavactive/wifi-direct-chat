package com.example.wifidirect.history;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.wifidirect.DB.ConversationModel;
import com.example.wifidirect.R;
import com.example.wifidirect.chat.ChatViewAdapter;

import java.util.List;

public class ConversationHistoryActivity extends AppCompatActivity implements DeleteHistoryDialog.DialogListener {

    private Toolbar toolbar;
    private ConversationHistViewModel viewModel;
    private RecyclerView conversationRecyclerView;
    private ChatViewAdapter chatViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversation_history);

        toolbar = findViewById(R.id.historyToolbar);
        conversationRecyclerView = findViewById(R.id.historyConversationRecyclerView);
        chatViewAdapter = new ChatViewAdapter();
        conversationRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        conversationRecyclerView.setAdapter(chatViewAdapter);

        setSupportActionBar(toolbar);
        if(getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorAccent));
        }
        viewModel = ViewModelProviders.of(this).get(ConversationHistViewModel.class);
        viewModel.getMessages().observe(this,this::setMessages);

        Intent intent = getIntent();
        long sessionId = intent.getLongExtra("SSID" , -1);
        viewModel.setDeviceName(intent.getStringExtra("deviceName"));
        viewModel.loadMessages(sessionId);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_remove) {
            DialogFragment deleteDialog = DeleteHistoryDialog.newInstance(viewModel.getDeviceName(), viewModel.getCurrentSSID());
            deleteDialog.show(getSupportFragmentManager(),"deleteSingleSession");
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setMessages(List<ConversationModel> conversationModels) {
        if(getSupportActionBar()!=null)
            getSupportActionBar().setTitle(viewModel.getDeviceName() + "(" + conversationModels.size() + ")");
        chatViewAdapter.messagesChanged(conversationModels);
    }

    @Override
    public void onDialogSuccess() {
        onBackPressed();
    }
}
