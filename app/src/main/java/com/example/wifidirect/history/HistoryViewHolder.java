package com.example.wifidirect.history;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.wifidirect.DB.SessionsModel;
import com.example.wifidirect.R;

public class HistoryViewHolder extends RecyclerView.ViewHolder {

    private TextView deviceName;
    private TextView startDate;
    private TextView endDate;
    private TextView messageCount;
    private View itemView;

    HistoryViewHolder(@NonNull View itemView) {
        super(itemView);
        deviceName = itemView.findViewById(R.id.historySessionDeviceName);
        startDate = itemView.findViewById(R.id.histroySessionStartDate);
        endDate = itemView.findViewById(R.id.histroySessionEndDate);
        messageCount = itemView.findViewById(R.id.historySessionMsgCount);
        this.itemView  = itemView;
    }

    void setData(SessionsModel sessionsModel, HistoryAdapter.ItemOnClickListener listener){
        deviceName.setText(sessionsModel.getDeviceName());
        startDate.setText(sessionsModel.getStartDateString());
        endDate.setText(sessionsModel.getEndDateString());
        messageCount.setText(String.valueOf(sessionsModel.getMessageCount()));
        itemView.setOnClickListener(view -> listener.onClick(sessionsModel.getSessionId(),sessionsModel.getDeviceName()));
        itemView.setOnLongClickListener(v -> {
            listener.onLongClick(sessionsModel.getSessionId(), sessionsModel.getDeviceName());
            return true;
        });
    }
}
