package com.example.wifidirect.history;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.wifidirect.DB.SessionsModel;
import com.example.wifidirect.R;

import java.util.ArrayList;
import java.util.List;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryViewHolder> {

    interface ItemOnClickListener{
        void onClick(Long sessionId, String deviceName);
        void onLongClick(Long sessionId, String deviceName);
    }

    private ItemOnClickListener listener;

    private List<SessionsModel> history = new ArrayList<>();

    HistoryAdapter(ItemOnClickListener listener){
        this.listener = listener;
    }

    @NonNull
    @Override
    public HistoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_history_item,parent,false);
        return new HistoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HistoryViewHolder holder, int position) {
        holder.setData(history.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return history.size();
    }

    void setHistory(List<SessionsModel> sessionsModels) {
        history.clear();
        history.addAll(sessionsModels);
        notifyDataSetChanged();
    }
}
