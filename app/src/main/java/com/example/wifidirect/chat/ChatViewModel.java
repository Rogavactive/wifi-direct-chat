package com.example.wifidirect.chat;

import android.app.Application;
import android.app.Service;
import android.content.Context;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.net.wifi.WpsInfo;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.wifidirect.DB.ChatDataBase;
import com.example.wifidirect.DB.ConversationModel;
import com.example.wifidirect.DB.SessionsModel;
import com.example.wifidirect.connection.MessageManager;
import com.example.wifidirect.connection.WifiDirectBroadcastReceiver;

import java.net.InetAddress;
import java.sql.Date;
import java.util.List;

public class ChatViewModel extends AndroidViewModel {

    public static final int DISCONNECTED = 0;
    public static final int CONNECTING = 1;
    public static final int CONNECTED = 2;
    private MutableLiveData<Integer> status = new MutableLiveData<>();

    private String clickedDeviceAddr;
    private String clickedDeviceName;

    private static ChatModel model;
    private static MessageManager messageManager;
    private static ChatDataBase db;

    private WifiManager wifiManager;
    private WifiP2pManager wifiP2pManager;
    private WifiP2pManager.Channel wifiP2pManagerChannel;

    private WifiDirectBroadcastReceiver broadcastReceiver;
    private MutableLiveData<WifiP2pDeviceList> peers = new MutableLiveData<>();
    private boolean isConnected = false;

    LiveData<WifiP2pDeviceList> getPeers() {
        return peers;
    }

    public ChatViewModel(@NonNull Application application) {
        super(application);
        model = new ChatModel();
        messageManager = new MessageManager();

        wifiManager = (WifiManager) application.getSystemService(Context.WIFI_SERVICE);
        wifiP2pManager = (WifiP2pManager) application.getSystemService(Service.WIFI_P2P_SERVICE);
        wifiP2pManagerChannel = wifiP2pManager.initialize(application, application.getMainLooper(), null);

        WifiP2pManager.PeerListListener peerListListener = peers -> ChatViewModel.this.peers.postValue(peers);
        broadcastReceiver = new WifiDirectBroadcastReceiver(wifiP2pManager, wifiP2pManagerChannel, peerListListener,
                this, status);
        IntentFilter mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);
        application.registerReceiver(broadcastReceiver, mIntentFilter);

        db = ChatDataBase.getInstance(application.getApplicationContext());
    }


    void searchPeers() {
        if (!wifiManager.isWifiEnabled())
            wifiManager.setWifiEnabled(true);
        wifiP2pManager.discoverPeers(wifiP2pManagerChannel, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
            }

            @Override
            public void onFailure(int reason) {
            }
        });
        broadcastReceiver.requestPeers();
    }

    void deviceClicked(String deviceAddr, String deviceName) {
        clickedDeviceAddr = deviceAddr;
        clickedDeviceName = deviceName;
        WifiP2pConfig config = new WifiP2pConfig();
        config.deviceAddress = deviceAddr;
        config.wps.setup = WpsInfo.PBC;
        status.setValue(CONNECTING);

        wifiP2pManager.connect(wifiP2pManagerChannel, config, new WifiP2pManager.ActionListener() {

            @Override
            public void onSuccess() {
                status.setValue(CONNECTED);
            }

            @Override
            public void onFailure(int reason) {
                status.setValue(DISCONNECTED);
            }
        });
    }

    private void messageReceived(String message) {
        new MessageProcessor(new ConversationModel(model.getSSID(), message, "R", new Date(System.currentTimeMillis()))).execute();
    }


    void sendMessage(String message) {
        new MessageProcessor(new ConversationModel(model.getSSID(), message, "S", new Date(System.currentTimeMillis()))).execute();
    }

    public void enableConnection(InetAddress groupOwnerAddress, boolean isGroupOwner) {
        if (!isConnected) {
            messageManager.init(groupOwnerAddress, isGroupOwner, this::messageReceived);
            isConnected = true;
        }
    }

    void disconnect() {
        if (wifiP2pManager != null && wifiP2pManagerChannel != null) {
            wifiP2pManager.requestGroupInfo(wifiP2pManagerChannel, group -> {
                if (group != null && wifiP2pManager != null && wifiP2pManagerChannel != null) {
                    wifiP2pManager.removeGroup(wifiP2pManagerChannel, new WifiP2pManager.ActionListener() {

                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onFailure(int reason) {
                        }
                    });
                }
            });
        }
        new SessionCloser().execute();
        messageManager.disconnect();
        isConnected = false;
    }

    void createNewSession() {
        if (status != null && status.getValue() != null && CONNECTED == status.getValue()) {
            wifiP2pManager.requestGroupInfo(wifiP2pManagerChannel, info -> {
                if(info!=null){
                    for(WifiP2pDevice device : info.getClientList()){
                        if(!device.deviceAddress.equals(broadcastReceiver.getThisDevice())){
                            model.setDeviceName(device.deviceName);
                            model.setMacAddress(device.deviceAddress);
                            new SessionCreator().execute();
                        }
                    }
                }else{
                    model.setDeviceName(clickedDeviceName);
                    model.setMacAddress(clickedDeviceAddr);
                }
                new SessionCreator().execute();
            });
        }
    }

    LiveData<List<ConversationModel>> getMessages() {
        return model.getMessages();
    }

    LiveData<String> getPeerDeviceName() {
        return model.getDeviceName();
    }

    LiveData<Integer> getStatus() {
        return status;
    }

    public void clear() {
        model.clearMessages();
    }

    public void setStatus(int status){
        this.status.postValue(status);
    }

    //Database calling Async tasks after this
    private static class SessionCreator extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            SessionsModel lastSession = new SessionsModel(model.getMacAddress(), model.getDeviceName().getValue(), new Date(System.currentTimeMillis()));
            long lastSessionId = db.chatDao().insertSession(lastSession);
            lastSession.setSessionId(lastSessionId);
            model.setCurrentSession(lastSession);
            return null;
        }
    }

    private static class SessionCloser extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            if (model.getCurrentSession() != null){
                model.getCurrentSession().setEndDate(new Date(System.currentTimeMillis()));
                db.chatDao().closeSession(model.getCurrentSession());
            }
            return null;
        }
    }

    private static class MessageProcessor extends AsyncTask<Void, Void, Void> {

        private ConversationModel message;

        MessageProcessor(ConversationModel message) {
            this.message = message;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            db.chatDao().insertMessage(message);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            model.addMessage(message);
            if (message.getSenderFlag().equals("S"))
                messageManager.send(message.getMessage());
        }
    }

}
