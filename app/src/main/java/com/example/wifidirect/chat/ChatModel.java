package com.example.wifidirect.chat;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.wifidirect.DB.ConversationModel;
import com.example.wifidirect.DB.SessionsModel;

import java.util.ArrayList;
import java.util.List;

public class ChatModel {
    private String macAddress;
    private MutableLiveData<String> deviceName = new MutableLiveData<>();
    private MutableLiveData<List<ConversationModel>> messages = new MutableLiveData<>();
    private SessionsModel currentSession;

    ChatModel(){
        messages.setValue(new ArrayList<>());
    }

    public void addMessage(ConversationModel message){
        List<ConversationModel> messageList = messages.getValue();
        if (messageList != null) {//Filled in constructor, so cannot be null.
            messageList.add(message);
        }
        messages.postValue(messageList);
    }

    public String getMacAddress() {
        return macAddress;
    }

    long getSSID(){
        return currentSession.getSessionId();
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public LiveData<String> getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName.postValue(deviceName);
    }

    public void setCurrentSession(SessionsModel session) {
        this.currentSession = session;
    }

    public SessionsModel getCurrentSession(){
        return currentSession;
    }

    public ConversationModel getMessage(int index) {
        return messages.getValue()!=null?messages.getValue().get(index):null;//Filled in constructor, so cannot be null.
    }

    public int getMessageCount(){
        return messages.getValue()!=null?messages.getValue().size():0;//Filled in constructor, so cannot be null.
    }

    public void clearMessages(){
        messages.setValue(new ArrayList<>());
    }

    public LiveData<List<ConversationModel>> getMessages(){
        return messages;
    }
}
