package com.example.wifidirect.chat;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.wifidirect.DB.ConversationModel;
import com.example.wifidirect.R;

class ChatViewHolder  extends RecyclerView.ViewHolder {

    private TextView messageText;
    private TextView messageTime;
    private boolean timeVisible = false;
    private Context context;

    ChatViewHolder(@NonNull View itemView) {
        super(itemView);
        messageText = itemView.findViewById(R.id.messageCell);
        messageTime = itemView.findViewById(R.id.messageTimeCell);
        messageTime.setVisibility(View.GONE);
        itemView.setOnLongClickListener(v -> {
            if(timeVisible){
                messageTime.setVisibility(View.GONE);
            }else {
                messageTime.setVisibility(View.VISIBLE);
            }
            timeVisible = !timeVisible;
            return true;
        });

        context = itemView.getContext();
    }

    @SuppressLint("SimpleDateFormat")
    public void setMessage(ConversationModel message) {
        messageText.setText(message.getMessage());
        messageTime.setText(message.getInputDateString());
        if(message.getSenderFlag().equals("S")){//I am sender

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(100, 16, 8, 0);
            params.gravity = Gravity.RIGHT;
            messageText.setLayoutParams(params);
            messageTime.setLayoutParams(params);
            messageText.setTextColor(Color.WHITE);

            messageText.setBackground(ContextCompat.getDrawable(context, R.drawable.my_message_shape));
        }else{//I am receiver

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(8, 16, 100, 0);
            params.gravity = Gravity.LEFT;
            messageText.setLayoutParams(params);
            messageTime.setLayoutParams(params);
            messageText.setTextColor(Color.BLACK);

            messageText.setBackground(ContextCompat.getDrawable(context, R.drawable.other_message_shape));
        }
    }



}
