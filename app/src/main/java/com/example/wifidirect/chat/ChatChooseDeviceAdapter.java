package com.example.wifidirect.chat;

import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.wifidirect.R;

import java.util.ArrayList;
import java.util.List;


public class ChatChooseDeviceAdapter extends RecyclerView.Adapter<ChooseDeviceViewHolder> {
    private List<WifiP2pDevice> data = new ArrayList<>();
    private ItemOnClickListener onClickListener;

    interface ItemOnClickListener{
        void onClick(String deviceAddr, String deviceName);
    }

    ChatChooseDeviceAdapter(ItemOnClickListener onClickListener){
        this.onClickListener = onClickListener;
    }

    @NonNull
    @Override
    public ChooseDeviceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view =  LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cell_device_info,parent,false);
        return new ChooseDeviceViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ChooseDeviceViewHolder holder, int position) {
        WifiP2pDevice item = data.get(position);
        holder.setData(item.deviceName,item.primaryDeviceType,item.status,item.deviceAddress,onClickListener);


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(WifiP2pDeviceList data){
        this.data.clear();
        this.data.addAll(data.getDeviceList());
        notifyDataSetChanged();
    }
}
