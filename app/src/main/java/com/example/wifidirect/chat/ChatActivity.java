package com.example.wifidirect.chat;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.example.wifidirect.R;

public class ChatActivity extends AppCompatActivity {

    private RecyclerView chooseRecyclerView;
    private ChatChooseDeviceAdapter chooseAdapter;
    private Button searchPeers;

    private RecyclerView chatView;
    private ChatViewAdapter chatViewAdapter;
    private EditText chatInput;
    private ImageView sendMessage;
    private ProgressBar loadingBar;
    private Toolbar toolbar;

    private ChatViewModel viewModel;

    private static final short LOCATION_PERMISSION_CODE = 4234;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        viewModel = ViewModelProviders.of(this).get(ChatViewModel.class);

        searchPeers = findViewById(R.id.searchPeersButton);
        searchPeers.setOnClickListener(v -> {
            if(ActivityCompat.checkSelfPermission(ChatActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED){
                viewModel.searchPeers();
            }else{
                ActivityCompat.requestPermissions(ChatActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_PERMISSION_CODE);
            }
        });
        chooseAdapter = new ChatChooseDeviceAdapter(this::showChat);
        chooseRecyclerView = findViewById(R.id.chooseRecyclerView);
        chooseRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        chooseRecyclerView.setAdapter(chooseAdapter);

        chatView = findViewById(R.id.chatRecyclerView);
        chatViewAdapter = new ChatViewAdapter();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setReverseLayout(true);
        chatView.setLayoutManager(linearLayoutManager);
        chatView.setAdapter(chatViewAdapter);
        chatView.setVisibility(View.GONE);
        toolbar = findViewById(R.id.chatToolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        chatInput = findViewById(R.id.messageEditText);
        chatInput.setVisibility(View.GONE);
        sendMessage = findViewById(R.id.sendMessageBtn);
        sendMessage.setVisibility(View.GONE);
        sendMessage.setOnClickListener(v -> {
            if(!chatInput.getText().toString().equals("")){
                viewModel.sendMessage(chatInput.getText().toString());
                chatInput.setText("");
            }
        });

        loadingBar = findViewById(R.id.loadingBar);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorAccent));
        }

        subscribeToData();
    }

    private void showChat(String deviceAddr, String deviceName) {
        viewModel.deviceClicked(deviceAddr, deviceName);
    }

    private void changeStatus(int status){
        switch (status){
            case ChatViewModel.CONNECTED:
                chooseRecyclerView.setVisibility(View.GONE);
                searchPeers.setVisibility(View.GONE);
                loadingBar.setVisibility(View.GONE);

                chatView.setVisibility(View.VISIBLE);
                chatInput.setVisibility(View.VISIBLE);
                sendMessage.setVisibility(View.VISIBLE);

                viewModel.createNewSession();
                break;
            case ChatViewModel.DISCONNECTED:

                chooseRecyclerView.setVisibility(View.VISIBLE);
                searchPeers.setVisibility(View.VISIBLE);
                loadingBar.setVisibility(View.GONE);
                if(getSupportActionBar()!=null) {
                    getSupportActionBar().setTitle("მოწყობილობები");
                }

                chatView.setVisibility(View.GONE);
                chatInput.setVisibility(View.GONE);
                sendMessage.setVisibility(View.GONE);
                chatInput.setText("");
                viewModel.clear();

                viewModel.disconnect();
                break;
            case ChatViewModel.CONNECTING:
                chooseRecyclerView.setVisibility(View.GONE);
                searchPeers.setVisibility(View.GONE);

                chatView.setVisibility(View.GONE);
                chatInput.setVisibility(View.GONE);
                sendMessage.setVisibility(View.GONE);
                if(getSupportActionBar()!=null)
                    getSupportActionBar().setTitle("მოწყობილობები");

                loadingBar.setVisibility(View.VISIBLE);
                break;

        }
    }

    private void subscribeToData() {
        viewModel.getPeers().observe(this,chooseAdapter::setData);
        viewModel.getMessages().observe(this,chatViewAdapter::messagesChanged);
        viewModel.getStatus().observe(this,this::changeStatus);
        viewModel.getPeerDeviceName().observe(this,this::setPeerDeviceName);
    }

    private void setPeerDeviceName(String peerDeviceName) {
        if(getSupportActionBar()!=null)
            getSupportActionBar().setTitle(peerDeviceName);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case LOCATION_PERMISSION_CODE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    viewModel.searchPeers();
                }
            }
        }
}

    @Override
    protected void onPause() {
        super.onPause();
        viewModel.setStatus(ChatViewModel.DISCONNECTED);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if(viewModel.getStatus()!=null&&viewModel.getStatus().getValue()!=null)
            switch (viewModel.getStatus().getValue()){
                case ChatViewModel.CONNECTED:
                    viewModel.setStatus(ChatViewModel.DISCONNECTED);
                    break;
                case ChatViewModel.DISCONNECTED:
                    super.onBackPressed();
                    break;
            }
    }
}
