package com.example.wifidirect.chat;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.wifidirect.DB.ConversationModel;
import com.example.wifidirect.R;

import java.util.ArrayList;
import java.util.List;

public class ChatViewAdapter extends RecyclerView.Adapter<ChatViewHolder> {

    private List<ConversationModel> data = new ArrayList<>();

    @NonNull
    @Override
    public ChatViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view =  LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cell_chat_message,parent,false);
        return new ChatViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ChatViewHolder holder, int position) {
        ConversationModel message = data.get(data.size() - position-1);
        holder.setMessage(message);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void messagesChanged(List<ConversationModel> messages){
        data = messages;
        notifyDataSetChanged();
    }

}
