package com.example.wifidirect.chat;

import android.net.wifi.p2p.WifiP2pDevice;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.wifidirect.R;

public class ChooseDeviceViewHolder extends RecyclerView.ViewHolder {

    private TextView deviceName;
    private TextView deviceType;
    private TextView deviceStatus;
    private ImageView deviceStatusIcon;
    private View itemView;

    public ChooseDeviceViewHolder(@NonNull View itemView) {
        super(itemView);
        deviceName = itemView.findViewById(R.id.cellDeviceName);
        deviceType = itemView.findViewById(R.id.cellDeviceType);
        deviceStatus = itemView.findViewById(R.id.cellStatus);
        deviceStatusIcon = itemView.findViewById(R.id.cellStatusIcon);
        this.itemView = itemView;
    }


    public void setData(String deviceNameVal, String deviceTypeVal, int deviceStatusVal, String deviceAddr
            , ChatChooseDeviceAdapter.ItemOnClickListener listener){
        deviceName.setText(deviceNameVal);
        deviceType.setText(deviceTypeVal);

        switch (deviceStatusVal){
            case WifiP2pDevice.CONNECTED:
                deviceStatusIcon.setColorFilter(R.color.colorConnected);
                deviceStatus.setText("connected");
                break;
            case WifiP2pDevice.INVITED:
                deviceStatusIcon.setColorFilter(R.color.colorInvited);
                deviceStatus.setText("invited");
                break;
            case WifiP2pDevice.FAILED:
                deviceStatusIcon.setColorFilter(R.color.colorFailed);
                deviceStatus.setText("failed");
                break;
            case WifiP2pDevice.AVAILABLE:
                deviceStatusIcon.setColorFilter(R.color.colorAvailable);
                deviceStatus.setText("available");
                break;
            case WifiP2pDevice.UNAVAILABLE:
                deviceStatusIcon.setColorFilter(R.color.colorUnAvailable);
                deviceStatus.setText("unavailable");
                break;
        }

        itemView.setOnClickListener(v -> listener.onClick(deviceAddr, deviceNameVal));
    }

}
