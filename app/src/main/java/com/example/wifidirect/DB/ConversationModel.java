package com.example.wifidirect.DB;

import android.annotation.SuppressLint;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.sql.Date;
import java.text.SimpleDateFormat;

@Entity(tableName = "conversations_history")
public class ConversationModel {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "session_id")
    private Long sessionId;

    @ColumnInfo(name = "message")
    private String message;

    @ColumnInfo(name = "sender_flag")
    private String senderFlag;

    @ColumnInfo(name = "inp_sysdate")
    private Date inputDate;

    public ConversationModel(Long sessionId, String message, String senderFlag, Date inputDate){
        this.sessionId = sessionId;
        this.message = message;
        this.senderFlag = senderFlag;
        this.inputDate = inputDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Long getSessionId() {
        return sessionId;
    }

    public void setSessionId(Long sessionId) {
        this.sessionId = sessionId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSenderFlag() {
        return senderFlag;
    }

    public void setSenderFlag(String senderFlag) {
        this.senderFlag = senderFlag;
    }

    public Date getInputDate() {
        return inputDate;
    }

    @SuppressLint("SimpleDateFormat")
    public String getInputDateString(){
        return new SimpleDateFormat("dd-MM-YY HH:mm:ss").format(inputDate);
    }

    public void setInputDate(Date inputDate) {
        this.inputDate = inputDate;
    }
}
