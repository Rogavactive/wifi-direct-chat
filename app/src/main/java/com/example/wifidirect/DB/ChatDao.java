package com.example.wifidirect.DB;


import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface ChatDao {

    @Query("select h.*, count(*) message_count from session_history h, conversations_history c where h.sessionId = c.session_id group by h.sessionId")
    List<SessionsModel> getSessions();

    @Insert
    long insertSession(SessionsModel session);

    @Query("select s.sessionId from session_history s where s.end_date is null")
    int getActiveSessionIdWithEndDate();

    @Query("select max(s.sessionId) from session_history s")
    int getActiveSessionIdWithMaxSessionId();

    @Query("select ifnull(max(sessionId), 0) + 1 from session_history")
    int getActiveSessionId();

    @Query("delete from session_history where sessionId = :sessionId")
    void deleteSession(long sessionId);

    @Query("delete from session_history")
    void deleteAllSessions();

    @Query("select count(1) from conversations_history h where h.session_id = :sessionId")
    int getMessageCount(int sessionId);

    @Query("select * from conversations_history h where h.session_id = :sessionId order by inp_sysdate desc")
    List<ConversationModel> getConversations(long sessionId);

    @Insert
    void insertMessage(ConversationModel message);

    @Update
    void closeSession(SessionsModel session);
}
