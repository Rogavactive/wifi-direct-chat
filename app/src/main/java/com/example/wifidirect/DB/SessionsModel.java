package com.example.wifidirect.DB;

import android.annotation.SuppressLint;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.sql.Date;
import java.text.SimpleDateFormat;

@Entity(tableName = "session_history")

public class SessionsModel {

    @PrimaryKey(autoGenerate = true)
    private Long sessionId;

    @ColumnInfo(name = "message_count")
    private int messageCount;

    @ColumnInfo(name = "mac_address")
    private String macAddress;

    @ColumnInfo(name = "device_name")
    private String deviceName;

    @ColumnInfo(name = "start_date")
    private Date startDate;

    @ColumnInfo(name = "end_date")
    private Date endDate;

    public SessionsModel(String macAddress, String deviceName, Date startDate){
        this.macAddress = macAddress;
        this.deviceName = deviceName;
        this.startDate = startDate;
    }

    public Long getSessionId() {
        return sessionId;
    }

    public void setSessionId(Long sessionId) {
        this.sessionId = sessionId;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }


    public int getMessageCount() {
        return messageCount;
    }

    @SuppressLint("SimpleDateFormat")
    public String getStartDateString(){
        if(getStartDate()==null)
            return "NULL";
        return new SimpleDateFormat("dd-MM-YY HH:mm:ss").format(getStartDate());
    }

    @SuppressLint("SimpleDateFormat")
    public String getEndDateString(){
        if(getEndDate()==null)
            return "NULL";
        return new SimpleDateFormat("dd-MM-YY HH:mm:ss").format(getEndDate());
    }

    public void setMessageCount(int mesageCount) {
        this.messageCount = mesageCount;
    }
}
