package com.example.wifidirect.DB;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

@Database(entities = {SessionsModel.class, ConversationModel.class}, version = 2, exportSchema = false)
@TypeConverters({DateTypeConverter.class})
public abstract class ChatDataBase extends RoomDatabase {
    private static final String DATABASE_NAME = "chat_db";
    private static ChatDataBase instacne;
    public abstract ChatDao chatDao();

    public static synchronized ChatDataBase getInstance(Context context){
        if(instacne == null){
            instacne = Room.databaseBuilder(context.getApplicationContext(),ChatDataBase.class, DATABASE_NAME).fallbackToDestructiveMigration().build();
        }
        return instacne;
    }

}
