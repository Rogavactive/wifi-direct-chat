package com.example.wifidirect.connection;

import java.net.InetAddress;

public class MessageManager {

    static final int PORT = 12817;
    private ConnectionManager connectionManager;

        public void disconnect() {
        if(connectionManager!=null)
            connectionManager.disconnect();
    }
    public interface MessageListener{
        void MessageReceived(String message);

    }

    public void init(InetAddress groupOwnerAddress, boolean isGroupOwner, MessageListener listener) {
        if(isGroupOwner){
            connectionManager = new ServerManager(listener);
        }else{
            connectionManager = new ClientManager(groupOwnerAddress,listener);
        }
        ((Thread) connectionManager).start();
    }

    public void send(String message) {
        connectionManager.send(message);
    }

}
