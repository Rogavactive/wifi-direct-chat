package com.example.wifidirect.connection;


import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Arrays;

class ClientManager extends Thread implements ConnectionManager {

    private Socket socket;
    private String hostAddress;
    private MessageManager.MessageListener listener;


    ClientManager(InetAddress hostAddress, MessageManager.MessageListener listener){
        this.hostAddress = hostAddress.getHostAddress();
        this.listener = listener;
    }

    @Override
    public void run() {
        try {
            socket = new Socket();
            socket.connect(new InetSocketAddress(hostAddress, MessageManager.PORT),500);//timeout necessary for server to be ready to accept.
            byte[] buffer = new byte[1024];
            while (socket!=null){
                int bytesRead = socket.getInputStream().read(buffer);
                if(bytesRead<0){
                    break;
                }
                if(bytesRead>0){
                    listener.MessageReceived(new String(Arrays.copyOfRange(buffer,0,bytesRead)));
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void send(String data) {
        new Thread(){
            @Override
            public void run() {
                try {
                    socket.getOutputStream().write(data.getBytes());
                    socket.getOutputStream().flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }catch (NullPointerException e){
                    e.printStackTrace();
                }
            }
        }.start();
    }

    @Override
    public void disconnect() {
        if(socket==null){
        }
        if(socket!=null){
            new Thread(){
                @Override
                public void run() {
                    try {
                        socket.close();//after socket close message read loop will terminate  - socket == null.
                    } catch (IOException e) {
                        e.printStackTrace();
                    }catch (NullPointerException e){
                        e.printStackTrace();
                    }
                }
            }.start();
        }
    }

}
