package com.example.wifidirect.connection;

public interface ConnectionManager extends Runnable{

    void send(String data);

    void disconnect();

}
