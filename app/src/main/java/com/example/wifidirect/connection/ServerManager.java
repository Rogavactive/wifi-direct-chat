package com.example.wifidirect.connection;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Arrays;

class ServerManager extends Thread implements ConnectionManager {

    private Socket socket;
    private ServerSocket serverSocket;
    private MessageManager.MessageListener listener;

    ServerManager(MessageManager.MessageListener listener) {
        this.listener = listener;
    }

    @Override
    public void run() {
        try {
            serverSocket = new ServerSocket(MessageManager.PORT);
            socket = serverSocket.accept();
            byte[] buffer = new byte[4096];
            while (socket!=null){
                int bytesRead = socket.getInputStream().read(buffer);
                if(bytesRead<0){
                    break;
                }
                if(bytesRead>0){
                    listener.MessageReceived(new String(Arrays.copyOfRange(buffer,0,bytesRead)));
                }
            }

        } catch (IOException e) {
        e.printStackTrace();
        }
    }

    @Override
    public void send(String data) {
        new Thread(){
            @Override
            public void run() {
                try {
                    socket.getOutputStream().write(data.getBytes());
                    socket.getOutputStream().flush();
                } catch (IOException e ) {
                    e.printStackTrace();
                } catch (NullPointerException e){
                    e.printStackTrace();
                }
            }
        }.start();
    }

    @Override
    public void disconnect() {
        if(socket!=null||serverSocket!=null){
            new Thread() {
                @Override
                public void run() {
                    try {
                        if(socket!=null){
                            socket.close();//after socket close message read loop will terminate  - socket == null.
                        }
                        if(serverSocket!=null){
                            serverSocket.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }catch (NullPointerException e){
                        e.printStackTrace();
                    }
                }
            }.start();
        }

    }

}
