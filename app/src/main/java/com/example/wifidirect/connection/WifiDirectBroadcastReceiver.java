package com.example.wifidirect.connection;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pManager;

import com.example.wifidirect.chat.ChatViewModel;

import androidx.lifecycle.MutableLiveData;

public class WifiDirectBroadcastReceiver extends BroadcastReceiver {

    private WifiP2pManager wifiP2pManager;
    private WifiP2pManager.Channel wifiP2pChannel;
    private WifiP2pManager.PeerListListener listener;
    private ChatViewModel chatViewModel;
    private MutableLiveData<Integer> status;
    private String thisDevice;

    public WifiDirectBroadcastReceiver(WifiP2pManager p2pManager,
                                       WifiP2pManager.Channel wifiP2pChannel,
                                       WifiP2pManager.PeerListListener listener,
                                       ChatViewModel chatViewModel,
                                       MutableLiveData<Integer> status){
        this.wifiP2pChannel = wifiP2pChannel;
        this.wifiP2pManager = p2pManager;
        this.listener = listener;
        this.chatViewModel = chatViewModel;
        this.status = status;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if(action==null)
            return;
        switch (action){
            case WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION:
                break;
            case WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION:
                requestPeers();
                break;
            case WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION:
                if(wifiP2pManager == null) return;

                NetworkInfo networkInfo = intent.getParcelableExtra(WifiP2pManager.EXTRA_NETWORK_INFO);
                if(networkInfo.isConnected()){
                    wifiP2pManager.requestConnectionInfo(wifiP2pChannel, info -> {
                        if(info.groupFormed){
                            status.setValue(ChatViewModel.CONNECTED);
                            chatViewModel.enableConnection(info.groupOwnerAddress, info.isGroupOwner);
                        }
                    });
                } else {
                    status.setValue(ChatViewModel.DISCONNECTED);
                }
                break;
            case WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION:
                WifiP2pDevice device =  intent
                        .getParcelableExtra(WifiP2pManager.EXTRA_WIFI_P2P_DEVICE);

                thisDevice = device.deviceAddress;
                break;
        }
    }

    public String getThisDevice(){
        return thisDevice;
    }

    public void requestPeers(){
        if(listener!=null&&wifiP2pChannel!=null)
            wifiP2pManager.requestPeers(wifiP2pChannel, listener);
    }
}
